from setuptools import setup, find_packages

setup(
    name="zlogfile",
    version="1.4",
    description="Compressed log handler",
    packages=['zlogfile'],
    install_requires=['msgpack==1.0.7', 'zstandard==0.22.0', 'brotli-file==0.1.0']
)

