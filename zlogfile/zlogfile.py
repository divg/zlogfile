import sys
import os
import io
import re

import msgpack
import json
import pickle

import lzma
import gzip
import bz2

try:
    import brotli_file # pip install brotli-file
    has_brotli = True
except ModuleNotFoundError:
    has_brotli = False


try:
    import zstandard # pip install zstandard
    has_zstandard = True
except ModuleNotFoundError:
    has_zstandard = False


### Format handlers
class JsonHandler:
    def __init__(self, fileobj):
        self.fileobj = fileobj
        self.text_stream = io.TextIOWrapper(fileobj, encoding='utf-8')
    def load(self):
        return json.loads(next(self.text_stream))
    def dump(self, obj):
        self.text_stream.write(json.dumps(obj))
        self.text_stream.write("\n")
    def close(self):
        self.text_stream.close()


class MsgpackHandler:
    def __init__(self, fileobj):
        self.unpacker = None
        self.fileobj = fileobj
        self.unpacker = msgpack.Unpacker(fileobj)
    def load(self):
        return next(self.unpacker)
    def dump(self, obj):
        self.fileobj.write(msgpack.dumps(obj))
    def close(self):
        pass


class PickleHandler:
    def __init__(self, fileobj):
        self.fileobj = fileobj
    def load(self):
        try:
            return pickle.load(self.fileobj)
        except EOFError as e:
            raise StopIteration(*e.args)

    def dump(self, obj):
        self.fileobj.write(pickle.dumps(obj))
    def close(self):
        pass


### Compression Handlers
class BaseCompressionHandler:
    def __init__(self, level):
        self.level = level
    def open(self, filename, mode):
        openfn = self.get_openfn()
        if self.level is not None and mode != 'rb':
            return openfn(filename, mode, **self.get_level_kwargs())
        else:
            return openfn(filename, mode)


class GzHandler(BaseCompressionHandler):
    levels = list(range(10))
    def get_openfn(self):
        return gzip.open
    def get_level_kwargs(self):
        return {'compresslevel': self.level}


class XzHandler(BaseCompressionHandler):
    levels = list(range(10))
    def get_openfn(self):
        return lzma.open
    def get_level_kwargs(self):
        return {'preset': self.level}


class NocompHandler(BaseCompressionHandler):
    levels = [None]
    def get_openfn(self):
        return open
    def get_level_kwargs(self):
        return {}


class Bz2Handler(BaseCompressionHandler):
    levels = list(range(1,10))
    def get_openfn(self):
        return bz2.open
    def get_level_kwargs(self):
        return {'compresslevel': self.level}


class BrotliHandler(BaseCompressionHandler):
    levels = list(range(12))
    def get_openfn(self):
        return brotli_file.open
    def get_level_kwargs(self):
        return {'quality': self.level}


class ZstdHandler(BaseCompressionHandler):
    levels = list(range(1,23))
    def get_openfn(self):
        return zstandard.open
    def get_level_kwargs(self):
        return {'cctx': zstandard.ZstdCompressor(level=self.level)}



class ZLogFile:
    compression_handlers = {  
        None:     NocompHandler,
        'gz':     GzHandler,
        'xz':     XzHandler,
        'bzip2':  Bz2Handler
    }

    if has_zstandard:
        compression_handlers['zst'] = ZstdHandler

    if has_brotli:
        compression_handlers['brotli'] = BrotliHandler

    format_handlers = {
        'json': JsonHandler,
        'msgpack': MsgpackHandler,
        'pickle': PickleHandler
    }

    formats = format_handlers.keys()
    compressors = compression_handlers.keys()

    @staticmethod
    def get_format_compressor(filename):
        filename_parts = filename.split('.')
        if filename_parts[-1] in ZLogFile.compressors:
            comp = filename_parts[-1]
            filename_parts.pop()
        else:
            comp = None
        fmt = filename_parts[-1]
        return (fmt, comp)

    def _call_open(self):
        format_handler = ZLogFile.compression_handlers[self.compressor_ext](level=self.level)
        if self.level is not None:
            assert self.level in format_handler.levels
        return format_handler.open(self.filename, self.mode)

    def _enforce_binary_mode(self):
        assert self.mode in ['w','r','a','wb','rb', 'ab']
        if len(self.mode) == 1:
            self.mode += 'b'

    def __init__(self, filename=None, mode='rb', level=None, \
        fileobj=None, format_id=None, compressor_id=None, echo_dumps=False):
        self.filename = filename
        self.mode = mode
        self.compressor_ext = None
        self.format_ext = None
        self.level = level
        self.echo_dumps = echo_dumps
        self._enforce_binary_mode()

        assert (format_id, compressor_id) == (None, None) or \
            (format_id and compressor_id), "Set both format_id & compressor_id or neither"

        if format_id and compressor_id:
            self.compressor_ext = compressor_id
            self.format_ext = format_id
        elif filename is not None:
            self.format_ext, self.compressor_ext = ZLogFile.get_format_compressor(filename)
        elif fileobj is not None and getattr(fileobj, "name", None) is not None:
            self.format_ext, self.compressor_ext = ZLogFile.get_format_compressor(fileobj.name)
        assert self.format_ext in ZLogFile.formats, f"Not supported format {self.format_ext}"
        assert self.compressor_ext in ZLogFile.compressors, f"Not supported compressor {self.compressor_ext}"

        # default level for zstd
        if self.compressor_ext == 'zst' and self.level == None:
            self.level = 8

        self.fileobj = self._call_open()
        self.handler = ZLogFile.format_handlers[self.format_ext](self.fileobj)

    def dump(self, obj):
        if self.echo_dumps:
            print(repr(obj))
        self.handler.dump(obj)

    def load(self):
        try:
            return self.handler.load()
        except StopIteration as e:
            raise EOFError(*e.args)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        self.handler.close()
        self.fileobj.close()

    def __iter__(self):
        return self

    def __next__(self):
        return self.handler.load()


def test_all():
    for compression in ZLogFile.compressors:
        levels = ZLogFile.compression_handlers[compression].levels
        if None not in levels:
            levels.append(None)
        for level in levels:
            for fm in ZLogFile.formats:
                filename = f'test.{fm}'
                if compression is not None:
                    filename += f'.{compression}'
                sys.stdout.write(f'testing {filename}: ')
                sys.stdout.write(f'\n')
                with ZLogFile(filename, 'wb', level=level) as f:
                    for i in range(10):
                        f.dump({str(i): i})

                with ZLogFile(filename, 'rb') as f:
                    for item in f:
                        sys.stdout.write(repr(item))
                    sys.stdout.write('\n')


def bench_data(data_lines):
    bench_levels = {
        None: [None],
        'zst': [1,3,5,7],
        'xz': [1,2,3],
        'bzip2': [1],
        'brotli': [1,2,3],
        'gz': [1,2,3]
    }
    def xstr(s):
        if s is None:
            return ''
        return str(s)

    from os import remove
    from os.path import getsize
    from time import time
    from os.path import isfile

    print('fmt,comp,lvl,size,time_r,time_w')
    for fmt in ZLogFile.formats:
        for compression in ZLogFile.compressors:
            #for level in ZLogFile.compression_handlers[compression].levels:
            for level in bench_levels[compression]:
                log_dir = os.getenv("LOGDIR",'.')
                log_fname = os.path.join(log_dir, f'logfiles_test')
                log_fname += f'.{fmt}'

                if compression is not None:
                    log_fname += f'.{compression}'
                if isfile(log_fname): remove(log_fname)
                t0 = time()
                with ZLogFile(log_fname, 'w', level=level) as fp:
                    for line in data_lines:
                        fp.dump(line)
                time_w = round(time() - t0,2)
                t0 = time()
                with ZLogFile(log_fname, 'r') as fp:
                    #i = 0
                    for line in fp:
                        pass
                        #assert line == data_lines[i]
                        #i += 1
                time_r = round(time() - t0,2)
                size = getsize(log_fname)
                print(f"{fmt},{xstr(compression)},{xstr(level)},{size},{time_r},{time_w}")


def bench_file(filename):
    with ZLogFile(filename, "rb") as f:
        data_lines = list(f)
    print(f"# filename = {filename}")
    return bench_data(data_lines)


def bench_intarray(n):
    def xstr(s):
        if s is None:
            return ''
        return str(s)
    print(f"# n = {n}")
    import random
    # generate test data: n lines of n random ascii digits with each line having one character random change at a random index
    data_lines = []
    row = [random.randint(0,99) for i in range(n)]
    for i in range(n):
        j = random.randint(0,n-1)
        row[j] = random.randint(0,99)
        data_lines.append([el for el in row])
    return bench_data(data_lines)

def open(filename=None, mode='rb', level=None, fileobj=None, format_id=None, compressor_id=None, echo_dumps=False):
    return ZLogFile(filename, mode, level, fileobj, format_id, compressor_id, echo_dumps)

if __name__ == '__main__':
    if re.match(r'^\d+$', sys.argv[1]):
        bench_intarray(int(sys.argv[1]))
    else:
        bench_file(sys.argv[1])
